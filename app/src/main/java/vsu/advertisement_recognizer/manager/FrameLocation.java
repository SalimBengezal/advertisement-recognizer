package vsu.advertisement_recognizer.manager;

import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;

import java.util.Date;

import vsu.recognition.entities.Frame;

/**
 * Класс, содержащий изображение и данные о местоположении и времени
 */
class FrameLocation extends Frame {

    private Double latitude;
    private Double longitude;
    private Date datetime;

    FrameLocation(Mat frame, MatOfKeyPoint keyPoints, MatOfKeyPoint descriptor, Double latitude, Double longitude, Date datetime) {
        super(frame, keyPoints, descriptor);
        this.latitude = latitude;
        this.longitude = longitude;
        this.datetime = datetime;
    }

    Date getDatetime() {
        return datetime;
    }

    Double getLatitude() {
        return latitude;
    }


    Double getLongitude() {
        return longitude;
    }
}
