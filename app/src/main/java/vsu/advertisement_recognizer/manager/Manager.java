package vsu.advertisement_recognizer.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import vsu.advertisement_recognizer.activities.SettingsActivity;
import vsu.advertisement_recognizer.db.Repository;
import vsu.advertisement_recognizer.db.model.Advertisement;
import vsu.advertisement_recognizer.db.model.Statistics;
import vsu.recognition.entities.DesiredObject;
import vsu.recognition.recognizer.Recognizer;

/**
 * Класс, управляющий всей работой приложения
 */
public class Manager {

    private final ArrayList<DesiredObject> desiredObjects;
    private FoundListener foundListener;
    private Repository repository;
    private FileManager fileManager;
    private Recognizer recognizer;
    private List<Advertisement> advertisements;

    public Manager(Context context, FoundListener listener) {
        fileManager = new FileManager(context);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        int minGoodPoints = Integer.parseInt(prefs.getString(SettingsActivity.CNT_BEST_MATCHES_KEY, ""));
        double ratio = (double) prefs.getInt(SettingsActivity.RATIO_KEY, -1) / 100;
        double homographThreshold = prefs.getInt(SettingsActivity.HOMOGRAPH_THRESHOLD_KEY, 3);
        int borderWidth = Integer.parseInt(prefs.getString(SettingsActivity.BORDER_WIDTH_KEY, ""));
        String borderColorPref = prefs.getString(SettingsActivity.BORDER_COLOR_KEY, "");
        Scalar borderColor = getColorScalar(borderColorPref);

        this.foundListener = listener;
        repository = new Repository();
        advertisements = repository.findAllAdvertisements();
        assert advertisements != null;
        desiredObjects = new ArrayList<>(advertisements.size());
        recognizer = new Recognizer(ratio, minGoodPoints, homographThreshold, borderWidth, borderColor);
        for (Advertisement advertisement : advertisements) {
            Mat frame = Imgcodecs.imread(advertisement.getImagePath());
            MatOfKeyPoint keypoints = recognizer.getKeyPoints(frame);
            MatOfKeyPoint descriptor = recognizer.getDescriptorByKeyPoints(frame, keypoints);
            DesiredObject object = new DesiredObject(frame.size(), keypoints, descriptor);
            desiredObjects.add(object);
        }
    }

    private Scalar getColorScalar(String colorString) {
        if (colorString.length() == 7 && colorString.substring(0,1).equals("#")) {
            String r = colorString.substring(1, 3);
            String g = colorString.substring(3, 5);
            String b = colorString.substring(5, 7);
            int rInt = Integer.parseInt(r, 16);
            int gInt = Integer.parseInt(g, 16);
            int bInt = Integer.parseInt(b, 16);
            return new Scalar(bInt, gInt, rInt);
        }
        return null;
    }

    public void recognize(Mat image, Double latitude, Double longitude, Date datetime) {
            new RecognizeTask(latitude, longitude, datetime).execute(image);
    }


    public interface FoundListener {
        void OnFound(Statistics statistics);
    }

    private class RecognizeTask extends AsyncTask<Mat, Void, Void> {

        private final String TAG = RecognizeTask.class.getName();
        private Double latitude;
        private Double longitude;
        private Date datetime;
        private List<Mat> results;
        private List<Integer> resultPositions;
        private FrameLocation frameLocation;

        RecognizeTask(Double latitude, Double longitude, Date datetime) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.datetime = datetime;
        }

        @Override
        protected Void doInBackground(Mat... mat) {
            Log.i(TAG, "Processing image");
            Mat image = mat[0];
            MatOfKeyPoint keypoints = recognizer.getKeyPoints(image);
            MatOfKeyPoint descriptor = recognizer.getDescriptorByKeyPoints(image, keypoints);
            frameLocation = new FrameLocation(image, keypoints, descriptor, latitude, longitude, datetime);

            resultPositions = new ArrayList<>(desiredObjects.size());
            results = new ArrayList<>(desiredObjects.size());

            for (int i = 0; i < desiredObjects.size(); i++) {
                Mat result = recognizer.Run(frameLocation, desiredObjects.get(i));
                if (result != null) {
                    resultPositions.add(i);
                    results.add(result);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.i(TAG, "Processed images. Checking matches");
            for (int i = 0; i < results.size(); i++) {
                Advertisement advertisement = advertisements.get(resultPositions.get(i));
                String fileName = fileManager.saveStatisticsImage(results.get(i), frameLocation.getDatetime(), advertisement.getId());
                Statistics statistics = new Statistics();
                statistics.setLongitude(frameLocation.getLongitude());
                statistics.setLatitude(frameLocation.getLatitude());
                statistics.setDateTime(frameLocation.getDatetime());
                statistics.setImagePath(fileName);
                repository.addStatisticsToAdvertisement(advertisement, statistics);
                foundListener.OnFound(statistics);
            }
        }
    }

}
