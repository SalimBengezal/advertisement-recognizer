package vsu.advertisement_recognizer.manager;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.util.Log;

import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Класс, работающий с файлами рекламы, миниатюр и т.д.
 */
public class FileManager {

    private static final String ADVERTISEMENT_DIR = "advertisements";
    private static final String STATISTICS_DIR = "statistics";
    private static final SimpleDateFormat DATETIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS", Locale.ENGLISH);

    private static final String TAG = FileManager.class.getName();

    private Context context;

    public FileManager(Context context) {
        this.context = context;
    }

    public String moveToAdvertisementDir(String path) {
        File from = new File(path);
        if (from.exists()) {
            File advertisementDir = new File(context.getExternalFilesDir(null), ADVERTISEMENT_DIR);
            if (!advertisementDir.exists() && !advertisementDir.mkdir())
                Log.e(TAG, "Can't create " + ADVERTISEMENT_DIR);
            File to = new File(advertisementDir, DATETIME_FORMAT.format(new Date()) + ".jpg");
            if (!from.renameTo(to))
                Log.e(TAG, "WARNING! File has not been renamed");
            else {
                        ThumbnailsMaker.createAndGetThumbnails(to);
                return to.getPath();
            }
        } else {
            Log.e(TAG, "File does not exist: " + path);
        }
        return null;
    }

    public void deleteImageWithThumbnails(String path) {
        File file = new File(path);
        if (!file.exists())
            Log.e(TAG, "File doesn't exist" + path);
        else {
            if (!file.delete()) {
                Log.e(TAG, "File has not been deleted");
            } else {
                ThumbnailsMaker.deleteThumbs(file);
            }
        }
    }

    public File getTemporaryAdvertisementFile() {
        File tempDir = new File(context.getExternalFilesDir(null), ADVERTISEMENT_DIR);
        if (!tempDir.exists() && !tempDir.mkdir()) {
            Log.e(TAG, "Can't create temporary dir");
            return null;
        } else {
            File temporaryFile = new File(tempDir, "temp.jpg");
            if (!temporaryFile.exists()) {
                try {
                    if (!temporaryFile.createNewFile())
                        Log.e(TAG, "Can't create temporary file");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return temporaryFile;
        }
    }

    public String saveStatisticsImage(Mat image, Date date, int id) {
        File statisticsDir = new File(context.getExternalFilesDir(null), STATISTICS_DIR);
        if (!statisticsDir.exists() && !statisticsDir.mkdir())
            Log.e(TAG, "Statistics dir has not been created: " + statisticsDir.getPath());

        String filePath = String.format("%s/%s_%s.jpg", statisticsDir.getPath(), id, DATETIME_FORMAT.format(date));
        Imgcodecs.imwrite(filePath, image);
        return filePath;
    }

    public Bitmap getThumbnails(File file) {
        return ThumbnailsMaker.createAndGetThumbnails(file);
    }

    private static class ThumbnailsMaker {

        private static final int SIZE = 64;

        static void deleteThumbs(File file) {
            File thumbDir = new File(file.getParent(), ".thumbs");
            if (thumbDir.exists()) {
                File thumbFile = new File(file.getParent(), ".thumbs/".concat(file.getName()));
                if (!thumbFile.exists())
                    Log.e(TAG, "File does not exist: " + thumbFile.getAbsolutePath());
                if (!thumbFile.delete()) {
                    Log.e(TAG, "WARNING! Thumbnail file has not been deleted");
                }
            }

        }

        static Bitmap createAndGetThumbnails(File file) {
            File thumbDir = new File(file.getParent(), ".thumbs");
            if (!thumbDir.exists()) {
                if (!thumbDir.mkdir()) {
                    Log.e(TAG, "Can not create thumb folder");
                }
            }
            File thumbFile = new File(thumbDir, file.getName());
            if (thumbFile.exists()) {
                return BitmapFactory.decodeFile(thumbFile.getPath());
            } else {
                Bitmap source = BitmapFactory.decodeFile(file.getPath());
                Bitmap thumbnails = ThumbnailUtils.extractThumbnail(source, SIZE, SIZE);
                FileOutputStream outputStream = null;
                try {
                    outputStream = new FileOutputStream(thumbFile.getPath());
                    thumbnails.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (outputStream != null) {
                            outputStream.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return thumbnails;
            }
        }
    }

}
