package vsu.advertisement_recognizer.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Модель таблицы рекламы в БД
 */
@DatabaseTable(tableName = Advertisement.TABLE_NAME)
public class Advertisement implements Serializable{

    static final String TABLE_NAME = "advertisement";

    private static final String FIELD_ID = "id";
    private static final String FIELD_IMAGE_PATH = "image_path";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_DESCRIPTION = "description";

    @DatabaseField(generatedId = true, columnName = FIELD_ID)
    private int id;
    @DatabaseField(columnName = FIELD_NAME, canBeNull = false)
    private String name;
    @DatabaseField(columnName = FIELD_DESCRIPTION)
    private String description;
    @DatabaseField(columnName = FIELD_IMAGE_PATH, canBeNull = false)
    private String imagePath;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
