package vsu.advertisement_recognizer.db.model;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;

/**
 * Модель таблицы найденного объекта в БД
 */
@DatabaseTable(tableName = Statistics.TABLE_NAME)
public class Statistics implements Serializable {

    public static final String FIELD_ID = "id";
    public static final String FIELD_ADVERTISEMENT_ID = "advertisement_id";
    public static final String FIELD_IMAGE_PATH = "image_path";
    public static final String FIELD_DATE_TIME = "date_time";
    public static final String FIELD_LATITUDE = "latitude";
    public static final String FIELD_LONGITUDE = "longitude";
    static final String TABLE_NAME = "statistics";
    @DatabaseField(generatedId = true, columnName = FIELD_ID)
    private int id;
    @DatabaseField(columnName = FIELD_ADVERTISEMENT_ID, canBeNull = false)
    private int advertisementId;
    @DatabaseField(columnName = FIELD_IMAGE_PATH, canBeNull = false)
    private String imagePath;
    @DatabaseField(columnName = FIELD_DATE_TIME)
    private Date dateTime;
    @DatabaseField(columnName = FIELD_LONGITUDE)
    private Double longitude;
    @DatabaseField(columnName = FIELD_LATITUDE)
    private Double latitude;

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAdvertisementId() {
        return advertisementId;
    }

    public void setAdvertisementId(int advertisementId) {
        this.advertisementId = advertisementId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
}
