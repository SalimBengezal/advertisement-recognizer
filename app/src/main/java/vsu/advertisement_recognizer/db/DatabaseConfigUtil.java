package vsu.advertisement_recognizer.db;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

import java.io.IOException;
import java.sql.SQLException;

import vsu.advertisement_recognizer.db.model.Advertisement;
import vsu.advertisement_recognizer.db.model.Statistics;

/**
 * Настройка конфигурации ORMLite
 */
public class DatabaseConfigUtil extends OrmLiteConfigUtil {

    private static final Class<?>[] classes = new Class[]{Advertisement.class, Statistics.class};

    public static void main(String[] args) throws SQLException, IOException {
        writeConfigFile("ormlite_config.txt", classes);
    }
}
