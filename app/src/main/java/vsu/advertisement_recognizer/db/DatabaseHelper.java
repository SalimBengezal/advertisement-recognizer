package vsu.advertisement_recognizer.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import vsu.advertisement_recognizer.R;
import vsu.advertisement_recognizer.db.model.Advertisement;
import vsu.advertisement_recognizer.db.model.Statistics;

/**
 * Вспомогательный класс для получения объектов из БД с помощью ORMLite
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "recognizer.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TAG = DatabaseHelper.class.getName();

    private Dao<Advertisement, Integer> advertisementDao = null;
    private Dao<Statistics, Integer> statisticsDao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            Log.i(TAG, "Creating tables");
            TableUtils.createTable(connectionSource, Advertisement.class);
            TableUtils.createTable(connectionSource, Statistics.class);
        } catch (SQLException e) {
            Log.e(TAG, "Can't create tables", e);
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(TAG, "Upgrading and dropping database");
            TableUtils.dropTable(connectionSource, Advertisement.class, true);
            TableUtils.dropTable(connectionSource, Statistics.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            Log.e(TAG, "Can't drop tables", e);
            throw new RuntimeException(e);
        }

    }

    public Dao<Advertisement, Integer> getAdvertisementDao() throws SQLException {
        if (advertisementDao == null)
            advertisementDao = getDao(Advertisement.class);
        return advertisementDao;
    }

    public Dao<Statistics, Integer> getStatisticsDao() throws SQLException {
        if (statisticsDao == null)
            statisticsDao = getDao(Statistics.class);
        return statisticsDao;
    }

    @Override
    public void close() {
        advertisementDao = null;
        statisticsDao = null;
        super.close();
    }
}
