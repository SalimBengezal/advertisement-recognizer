package vsu.advertisement_recognizer.db;

import android.util.Log;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import vsu.advertisement_recognizer.db.model.Advertisement;
import vsu.advertisement_recognizer.db.model.Statistics;

/**
 * Класс DAO
 */
public class Repository {

    private final static String TAG = Repository.class.getName();

    private final DatabaseHelper helper = HelperFactory.getHelper();

    public int createAdvertisement(Advertisement advertisement){
        int index = -1;
        try {
            index = helper.getAdvertisementDao().create(advertisement);
        }catch (SQLException e){
            Log.e(TAG, "Error while creating: " + advertisement.getName());
        }
        return index;
    }

    public int updateAdvertisement(Advertisement advertisement){
        int index = -1;
        try {
            index = helper.getAdvertisementDao().update(advertisement);
        }catch (SQLException e){
            Log.e(TAG, "Error while updating: " + advertisement.getName());
        }
        return index;
    }

    public int deleteAdvertisement(Advertisement advertisement){
        int index = -1;
        try {
            index = helper.getAdvertisementDao().delete(advertisement);
        }catch (SQLException e){
            Log.e(TAG, "Error while deleting: " + advertisement.getName());
        }
        return index;
    }

    public Advertisement findAdvertisementById(int id){
        Advertisement advertisement=null;
        try {
            advertisement = helper.getAdvertisementDao().queryForId(id);
        }catch (SQLException e){
            Log.e(TAG, "Error while searching: id=" + id);
        }
        return advertisement;
    }

    public List<Advertisement> findAllAdvertisements(){
        List<Advertisement> advertisements = null;
        try {
            advertisements=helper.getAdvertisementDao().queryForAll();
        }catch (SQLException e){
            Log.e(TAG, "Error while getting all advertisements");
        }
        return advertisements;
    }

    public Collection<Statistics> getStatisticsByAdvertisementId(int id){
        try {
            return helper.getStatisticsDao().queryBuilder().where().eq(Statistics.FIELD_ADVERTISEMENT_ID, id).query();
        } catch (SQLException e) {
            Log.e(TAG, "Error while getting all statistics by advertisement: id=" + id);
        }
        return null;
    }

    public void clearStatisticsByAdvertisementId(int id){
        Collection<Statistics> statistics;
        try {
            statistics = helper.getStatisticsDao().queryBuilder().where().eq(Statistics.FIELD_ADVERTISEMENT_ID, id).query();
            helper.getStatisticsDao().delete(statistics);
        } catch (SQLException e) {
            Log.e(TAG, "Error while clearing statistics by advertisement: id=" + id);
        }
    }

    public int addStatisticsToAdvertisement(Advertisement advertisement, Statistics statistics){
        int index = -1;
        try {
            statistics.setAdvertisementId(advertisement.getId());
            index = helper.getStatisticsDao().create(statistics);
        }catch (SQLException e){
            Log.e(TAG, "Error while adding statistic to advertisement: id=" + advertisement.getId());
        }
        return index;
    }

    public Statistics findStatisticsById(int id){
        Statistics statistics=null;
        try {
            statistics = helper.getStatisticsDao().queryForId(id);
        }catch (SQLException e){
            Log.e(TAG, "Error while searching: id=" + id);
        }
        return statistics;
    }
}
