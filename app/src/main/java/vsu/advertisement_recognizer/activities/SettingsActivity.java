package vsu.advertisement_recognizer.activities;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import vsu.advertisement_recognizer.R;
import vsu.advertisement_recognizer.fragments.PreferenceFragment;

/**
 * Класс, описывающий экран настроек
 */
public class SettingsActivity extends AppCompatActivity {

    public static String RATIO_KEY = "good_matches_ratio";
    public static String BORDER_COLOR_KEY = "border_color";
    public static String BORDER_WIDTH_KEY = "border_width";
    public static String HOMOGRAPH_THRESHOLD_KEY = "homograph_threshold";
    public static String CNT_BEST_MATCHES_KEY = "number_best_matches";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new PreferenceFragment())
                .commit();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_set_default:
                //TODO: Check
                PreferenceManager.setDefaultValues(this, R.xml.preferences, true);
                break;
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
