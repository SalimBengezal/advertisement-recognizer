package vsu.advertisement_recognizer.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

import vsu.advertisement_recognizer.R;
import vsu.advertisement_recognizer.adapters.StatisticsAdapter;
import vsu.advertisement_recognizer.db.Repository;
import vsu.advertisement_recognizer.db.model.Advertisement;
import vsu.advertisement_recognizer.db.model.Statistics;

/**
 * Класс, описывающий работу экрана подробного описания рекламы
 */
public class AdvertisementDetailsActivity extends AppCompatActivity {

    private static final String TAG = AdvertisementDetailsActivity.class.getName();

    public AdvertisementDetailsActivity() {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertisement_details);
        final ListView statisticsList1 = (ListView) findViewById(R.id.activity_advertisement_details_statistics_list);
        statisticsList1.setEmptyView(findViewById(android.R.id.empty));

        TextView tvTitle = (TextView) findViewById(R.id.activity_advertisement_details_name);
        TextView tvDescription = (TextView) findViewById(R.id.activity_advertisement_details_description);
        ImageView ivImage = (ImageView) findViewById(R.id.activity_advertisement_details_image);
        ImageButton ivStatisticsOnMap = (ImageButton) findViewById(R.id.activity_advertisement_details_statistics_on_map);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        int id = getIntent().getIntExtra("id", -1);
        if (id != -1) {

            final Repository repository = new Repository();
            final Advertisement advertisement = repository.findAdvertisementById(id);
            if (advertisement != null) {
                ivImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.parse("file://" + advertisement.getImagePath()), "image/*");
                        startActivity(intent);
                    }
                });
                tvTitle.setText(advertisement.getName());
                tvDescription.setText(advertisement.getDescription());
                ivStatisticsOnMap.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (statisticsList1.getAdapter().getCount() > 0) {
                            Intent locationIntent = new Intent(getApplicationContext(), MapsActivity.class);
                            locationIntent.putExtra("adv_id", advertisement.getId());
                            startActivity(locationIntent);
                        }
                    }
                });
                File imageFile = new File(advertisement.getImagePath());
                if (!imageFile.exists()) {
                    Log.e(TAG, "Image file does not exist");
                } else {
                    Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getPath());
                    ivImage.setImageBitmap(bitmap);
                }
                final ArrayList<Statistics> statisticsList = new ArrayList<>(repository.getStatisticsByAdvertisementId(id));
                StatisticsAdapter statisticsAdapter = new StatisticsAdapter(getBaseContext(), statisticsList);
                statisticsList1.setAdapter(statisticsAdapter);
                statisticsList1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                        Statistics statistics = statisticsList.get(pos);
                        int id = (int) statistics.getId();
                        if (statistics.getLatitude() != null && statistics.getLongitude() != null) {
                            Intent locationIntent = new Intent(getApplicationContext(), MapsActivity.class);
                            locationIntent.putExtra("stat_id", id);
                            startActivity(locationIntent);
                        } else {
                            Toast.makeText(getApplicationContext(), "Местоположение не определено", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }

    }

}
