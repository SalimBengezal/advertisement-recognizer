package vsu.advertisement_recognizer.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;

import vsu.advertisement_recognizer.R;
import vsu.advertisement_recognizer.db.Repository;
import vsu.advertisement_recognizer.db.model.Advertisement;
import vsu.advertisement_recognizer.db.model.Statistics;
import vsu.advertisement_recognizer.manager.FileManager;

/**
 * Класс, описывающий экран с картой Google
 */
public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final DateFormat DATETIME_FORMAT = DateFormat.getDateTimeInstance();
    private ArrayList<Statistics> statisticsList;
    private FileManager fileManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        Repository repository = new Repository();
        int advertisementId = getIntent().getIntExtra("adv_id", -1);
        int statisticId = getIntent().getIntExtra("stat_id", -1);
        if (advertisementId != -1) {
            Advertisement advertisement = repository.findAdvertisementById(advertisementId);
            if (advertisement != null) {
                statisticsList = new ArrayList<>(repository.getStatisticsByAdvertisementId(advertisement.getId()));
            }
        } else if (statisticId != -1) {
            Statistics statistics = repository.findStatisticsById(statisticId);
            statisticsList = new ArrayList<>();
            statisticsList.add(statistics);
        }
        fileManager = new FileManager(getBaseContext());

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (!statisticsList.isEmpty()) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Statistics statistic : statisticsList) {
                if (statistic.getLongitude()!=null && statistic.getLatitude()!=null) {
                    LatLng place = new LatLng(statistic.getLatitude(), statistic.getLongitude());
                    Marker marker = googleMap.addMarker(new MarkerOptions().position(place).title(DATETIME_FORMAT.format(statistic.getDateTime()))
                            .icon(BitmapDescriptorFactory.fromBitmap(fileManager.getThumbnails(new File(statistic.getImagePath())))));
                    builder.include(marker.getPosition());
                }
            }
            LatLngBounds bounds = builder.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 100,100,10);
            googleMap.animateCamera(cu);
        }
    }

}
