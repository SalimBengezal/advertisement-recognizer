package vsu.advertisement_recognizer.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import vsu.advertisement_recognizer.R;
import vsu.advertisement_recognizer.adapters.WatchingStatisticAdapter;
import vsu.advertisement_recognizer.db.model.Statistics;
import vsu.advertisement_recognizer.manager.Manager;

/**
 * Класс, описывающий экран наблюдения
 */
public class WatchingActivity extends AppCompatActivity implements Manager.FoundListener {

    private WatchingStatisticAdapter adapter;
    private boolean doubleBackToExitPressedOnce = false;
    private ListView listview;
    private ArrayList<Statistics> statisticsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (getActionBar() != null)
            getActionBar().hide();
        setContentView(R.layout.activity_watching);
        listview = (ListView) findViewById(R.id.activity_watching_list);
        statisticsList = new ArrayList<>();
        adapter = new WatchingStatisticAdapter(getBaseContext(), statisticsList);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse("file://" + statisticsList.get(pos).getImagePath()), "image/*");
                startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.watching_double_click_to_exit, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public void OnFound(Statistics statistics) {

        Toast.makeText(getBaseContext(), "НАЙДЕНО!!!!", Toast.LENGTH_SHORT).show();
        statisticsList.add(statistics);
        adapter.notifyDataSetChanged();
        listview.setSelection(adapter.getCount() - 1);
    }
}
