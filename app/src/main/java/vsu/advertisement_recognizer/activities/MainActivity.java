package vsu.advertisement_recognizer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import vsu.advertisement_recognizer.R;
import vsu.advertisement_recognizer.db.Repository;

/**
 * Класс, описывающий главный экран
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnStartWatching;
    private Repository repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        setContentView(R.layout.activity_main);
        btnStartWatching = (Button) findViewById(R.id.activity_main_btn_watching);
        Button btnExploreAdvertisements = (Button) findViewById(R.id.activity_main_btn_advertisements);
        btnStartWatching.setOnClickListener(this);
        btnExploreAdvertisements.setOnClickListener(this);
        repository = new Repository();
    }

    @Override
    protected void onResume() {
        super.onResume();
        btnStartWatching.setEnabled(repository.findAllAdvertisements().size() > 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_main_btn_watching:
                startActivity(new Intent(this, WatchingActivity.class));
                break;
            case R.id.activity_main_btn_advertisements:
                startActivity(new Intent(this, AdvertisementsActivity.class));
                break;
            default:
                break;
        }
    }
}
