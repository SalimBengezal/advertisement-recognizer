package vsu.advertisement_recognizer.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import vsu.advertisement_recognizer.R;
import vsu.advertisement_recognizer.adapters.AdvertisementAdapter;
import vsu.advertisement_recognizer.db.Repository;
import vsu.advertisement_recognizer.db.model.Advertisement;
import vsu.advertisement_recognizer.db.model.Statistics;
import vsu.advertisement_recognizer.manager.FileManager;

/**
 * Класс, описывающий экран базы рекламы
 */
public class AdvertisementsActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private static final int CM_DELETE_ID = 1;
    private static final String TAG = AdvertisementDetailsActivity.class.getName();
    private static final int REQUEST_PICTURE_CROP = 3;
    private static final int REQUEST_IMAGE_CAPTURE = 2;
    private static final int REQUEST_IMAGE_ADDED = 4;
    private static final int REQUEST_CAMERA_PERMISSIONS = 5;
    private ListView advertisementList;
    private Repository repository;
    private FileManager fileManager;
    private Uri pictureURI;
    private AdvertisementAdapter advertisementAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertisements);
        fileManager = new FileManager(getBaseContext());
        advertisementList = (ListView) findViewById(R.id.activity_advertisements_list);
        repository = new Repository();
        List<Advertisement> advertisements = repository.findAllAdvertisements();
        advertisementAdapter = new AdvertisementAdapter(getBaseContext(), advertisements);
        advertisementList.setAdapter(advertisementAdapter);
        advertisementList.setOnItemClickListener(this);
        registerForContextMenu(advertisementList);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        FloatingActionButton newPhotoFab = (FloatingActionButton) findViewById(R.id.activity_advertisements_fab_new_photo);
        newPhotoFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCaptureIntentActivity();
            }
        });

        advertisementList.setEmptyView(findViewById(android.R.id.empty));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void startCaptureIntentActivity() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        int permission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        if (permission == PackageManager.PERMISSION_GRANTED)
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        else
            checkCameraPermissions();
    }

    private void checkCameraPermissions() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(getBaseContext(), "Необходимо разрешение использования камеры", Toast.LENGTH_SHORT).show();
                } else
                    startCaptureIntentActivity();
                break;
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, CM_DELETE_ID, 0, R.string.advertisements_cm_delete);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        super.onContextItemSelected(item);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case CM_DELETE_ID:
                Advertisement advertisement = advertisementAdapter.getItem(info.position);
                assert advertisement != null;
                List<Statistics> statistics = new ArrayList<>(repository.getStatisticsByAdvertisementId(advertisement.getId()));
                repository.deleteAdvertisement(advertisement);
                fileManager.deleteImageWithThumbnails(advertisement.getImagePath());
                for (Statistics statistic : statistics) {
                    fileManager.deleteImageWithThumbnails(statistic.getImagePath());
                }
                repository.clearStatisticsByAdvertisementId(advertisement.getId());
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case RESULT_OK:
                switch (requestCode) {
                    case REQUEST_IMAGE_CAPTURE:
                        pictureURI = data.getData();
                        performCrop();
                        break;
                    case REQUEST_PICTURE_CROP:
                        Bundle extras = data.getExtras();
                        Uri uri = extras.getParcelable("output");
                        assert uri != null;
                        Intent newAdvertisementIntent = new Intent(this, NewAdvertisementActivity.class);
                        newAdvertisementIntent.putExtra("image", uri.getPath());
                        startActivityForResult(newAdvertisementIntent, REQUEST_IMAGE_ADDED);
                        break;
                    case REQUEST_IMAGE_ADDED:
                        List<Advertisement> advertisements = repository.findAllAdvertisements();
                        advertisementAdapter = new AdvertisementAdapter(getBaseContext(), advertisements);
                        advertisementList.setAdapter(advertisementAdapter);
                        break;
                }
                break;
            case RESULT_CANCELED:

                // TODO: Delete temporary file in DCIM directory
                if (requestCode == REQUEST_PICTURE_CROP || requestCode == REQUEST_IMAGE_ADDED) {
                /*File tempFile = new File(getExternalFilesDir(null), TEMP_CAPTURED_FILE);
                if (!tempFile.delete())
                    Log.e(TAG, "Can not delete temporary file");*/
                }
                break;
        }
    }

    private void performCrop() {
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        cropIntent.setDataAndType(pictureURI, "image/*");
        cropIntent.putExtra("crop", true);
        cropIntent.putExtra("return-listData", false);
        cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileManager.getTemporaryAdvertisementFile()));
        startActivityForResult(cropIntent, REQUEST_PICTURE_CROP);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent objectDetailsIntent = new Intent(this, AdvertisementDetailsActivity.class);
        Advertisement advertisement = (Advertisement) parent.getItemAtPosition(position);
        objectDetailsIntent.putExtra("id", advertisement.getId());
        startActivity(objectDetailsIntent);
    }
}
