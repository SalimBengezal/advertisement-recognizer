package vsu.advertisement_recognizer.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import vsu.advertisement_recognizer.R;
import vsu.advertisement_recognizer.db.Repository;
import vsu.advertisement_recognizer.db.model.Advertisement;
import vsu.advertisement_recognizer.manager.FileManager;

/**
 * Класс, описывающий экран добавления новой рекламы
 */
public class NewAdvertisementActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = NewAdvertisementActivity.class.getName();
    private TextView tvName;
    private TextView tvDescription;
    private String imagePath;
    private Repository repository;
    private FileManager fileManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_advertisement);
        ImageView ivImage = (ImageView) findViewById(R.id.activity_new_advertisement_image);
        imagePath = getIntent().getStringExtra("image");

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);
        ivImage.setImageBitmap(bitmap);

        tvName = (TextView) findViewById(R.id.activity_new_advertisement_name);
        tvDescription = (TextView) findViewById(R.id.activity_new_advertisement_description);
        Button btnAdd = (Button) findViewById(R.id.activity_new_advertisement_btn_add);
        btnAdd.setOnClickListener(this);
        repository = new Repository();
        fileManager = new FileManager(getBaseContext());

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_new_advertisement_btn_add:
                String name = tvName.getText().toString();
                String description = tvDescription.getText().toString();
                if (name.trim().length() >= 2) {
                    String filepath = fileManager.moveToAdvertisementDir(imagePath);
                    Advertisement advertisement = new Advertisement();
                    advertisement.setName(name);
                    advertisement.setImagePath(filepath);
                    advertisement.setDescription(description);
                    repository.createAdvertisement(advertisement);
                    setResult(RESULT_OK);
                    finish();
                } else {
                    Toast.makeText(this, R.string.new_advertisement_too_short_name_message, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
