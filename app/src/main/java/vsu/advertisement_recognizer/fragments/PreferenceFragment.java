package vsu.advertisement_recognizer.fragments;

import android.os.Bundle;

import vsu.advertisement_recognizer.R;

/**
 * Фрагмент настроек
 */
public class PreferenceFragment extends android.preference.PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}