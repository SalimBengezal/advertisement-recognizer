package vsu.advertisement_recognizer.fragments;


import android.Manifest;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.util.Date;
import java.util.Locale;

import vsu.advertisement_recognizer.R;
import vsu.advertisement_recognizer.manager.Manager;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Класс, описывающий фрагмент камеры
 */
public class CameraFragment extends Fragment implements CameraBridgeViewBase.CvCameraViewListener2 {

    private static final String TAG = CameraFragment.class.getName();
    private static final int REQUEST_LOCATION_ACCESS = 1;
    private static final int REQUEST_WRITE_EXTERNAL = 2;
    private Manager manager;
    private CameraBridgeViewBase openCVCameraView;
    private BaseLoaderCallback loaderCallback;
    private Mat frame;
    private LocationManager locationManager;
    private Location currentLocation;
    private TextView tvLocationInfo;
    private ImageView ivGpsStatus;

    private final LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            currentLocation = location;
            updateGpsStatus();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
            checkGPSPermission();
            currentLocation = locationManager.getLastKnownLocation(provider);
            updateGpsStatus();
        }

        @Override
        public void onProviderDisabled(String provider) {
            askGpsIn5Seconds();
            updateGpsStatus();
        }
    };

    private Handler processHandler = new Handler();
    private Runnable processRunnable = new Runnable() {
        @Override
        public void run() {
            Log.i(TAG, "Attempt to process");
            if (manager != null) {
                Log.e(TAG, "Sending image to manager process");
                if (currentLocation != null)
                    manager.recognize(frame.clone(), currentLocation.getLatitude(), currentLocation.getLongitude(), new Date());
                else
                    manager.recognize(frame.clone(), null, null, new Date());
            }
            processHandler.postDelayed(this, 1000);
        }
    };
    private Manager.FoundListener foundListener;
    private ProgressDialog progressDialog;

    private static String locationToString(Location location) {
        if (location == null)
            return "";
        return String.format(Locale.ENGLISH,
                "Latitude = %1$.4f, longitude = %2$.4f, time = %3$tF %3$tT",
                location.getLatitude(), location.getLongitude(), new Date(location.getTime()));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        progressDialog = new ProgressDialog(getActivity());
        loaderCallback = new BaseLoaderCallback(context) {
            @Override
            public void onManagerConnected(int status) {
                switch (status) {
                    case LoaderCallbackInterface.SUCCESS: {
                        Log.d(TAG, "OpenCV loaded successfully");
                        openCVCameraView.enableView();
                        new InitializerAsync().execute();
                        break;
                    }
                    default:
                        super.onManagerConnected(status);
                        break;
                }
            }
        };
        foundListener = (Manager.FoundListener) getActivity();

    }

    private void updateGpsStatus() {
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            ivGpsStatus.setImageDrawable(ContextCompat.getDrawable(getActivity().getBaseContext(), R.drawable.ic_gps_off_red_24dp));
        } else {
            if (currentLocation != null)
                ivGpsStatus.setImageDrawable(ContextCompat.getDrawable(getActivity().getBaseContext(), R.drawable.ic_gps_fixed_green_24dp));
            else
                ivGpsStatus.setImageDrawable(ContextCompat.getDrawable(getActivity().getBaseContext(), R.drawable.ic_gps_not_fixed_orange_24dp));
        }
        tvLocationInfo.setText(locationToString(currentLocation));
    }

    private void askGpsIn5Seconds() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Необходимо включить геоданные для сбора полноценной статистики")
                        .setPositiveButton("Включить", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        })
                        .setNegativeButton("Выход", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                getActivity().finish();
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }, 5000);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_camera, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        openCVCameraView = (JavaCameraView) view.findViewById(R.id.fragment_camera_camera_view);
        ivGpsStatus = (ImageView) view.findViewById(R.id.fragment_camera_gps_status_image);
        tvLocationInfo = (TextView) view.findViewById(R.id.fragment_camera_location_info);
        openCVCameraView.setCvCameraViewListener(this);
    }

    @Override
    public void onPause() {
        Log.i(TAG, "onPause");
        super.onPause();
        if (openCVCameraView != null)
            openCVCameraView.disableView();
        locationManager.removeUpdates(locationListener);
        processHandler.removeCallbacks(processRunnable);
    }

    private void checkGPSPermission() {
        if (ActivityCompat.checkSelfPermission(getActivity().getBaseContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_ACCESS);
        }
    }

    private void checkWriteExternalPermission() {
        if (ActivityCompat.checkSelfPermission(getActivity().getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL);
        }
    }

    private void startLocationListener() {
        int permission = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION);
        if (permission == PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Location listener granted");
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, locationListener);
        } else {
            checkGPSPermission();
        }
    }

    private void askPermissionBeforeWritingExternal() {
        int permission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission == PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Write external storage granted");
        } else {
            checkWriteExternalPermission();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_2_0, getActivity().getApplicationContext(), loaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it");
            loaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
        startLocationListener();
        askPermissionBeforeWritingExternal();
        processHandler.postDelayed(processRunnable, 4000);
        updateGpsStatus();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i(TAG, "Received Permission Result");
        switch (requestCode) {
            case REQUEST_LOCATION_ACCESS:
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(getActivity(), "Для ведения статистики необходимо включить геоданные", Toast.LENGTH_LONG).show();
                    getActivity().finish();
                } else
                    onResume();
                break;
            case REQUEST_WRITE_EXTERNAL:
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(getActivity(), "Для ведения статистики необходимо разрешить сохранение во внутреннюю память", Toast.LENGTH_LONG).show();
                    getActivity().finish();
                } else
                    onResume();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (openCVCameraView != null)
            openCVCameraView.disableView();
        manager = null;
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        Log.i(TAG, "Camera View Started");
        frame = new Mat(height, width, CvType.CV_8UC4);
    }

    @Override
    public void onCameraViewStopped() {
        Log.i(TAG, "Camera View stopped");
        frame.release();
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        Mat rgba = inputFrame.rgba();
        Imgproc.cvtColor(rgba, frame, Imgproc.COLOR_RGB2BGR);
        return rgba;
    }

    private class InitializerAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setTitle("Инициализация...");
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            manager = new Manager(getActivity(), foundListener);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
        }
    }

}
