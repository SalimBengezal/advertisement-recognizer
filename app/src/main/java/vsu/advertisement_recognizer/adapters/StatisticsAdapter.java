package vsu.advertisement_recognizer.adapters;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import vsu.advertisement_recognizer.R;
import vsu.advertisement_recognizer.db.model.Statistics;
import vsu.advertisement_recognizer.manager.FileManager;

/**
 * Класс адаптера списка статистики для экрана подробного описания
 */
public class StatisticsAdapter extends ArrayAdapter<Statistics> {

    private static final DateFormat DATE_FORMAT = SimpleDateFormat.getDateInstance();
    private static final DateFormat TIME_FORMAT = SimpleDateFormat.getTimeInstance();
    private static final String TAG = StatisticsAdapter.class.getName();
    private FileManager fileManager;

    public StatisticsAdapter(Context context, List<Statistics> statisticsList) {
        super(context, 0, statisticsList);
        fileManager = new FileManager(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final Statistics statistics = getItem(position);
        if (convertView == null)
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_advertisement_statistics, parent, false);
        TextView tvDate = (TextView) convertView.findViewById(R.id.item_adv_statistics_date);
        TextView tvTime = (TextView) convertView.findViewById(R.id.item_adv_statistics_time);
        TextView tvAddress = (TextView) convertView.findViewById(R.id.item_adv_statistics_address);
        ImageView ivImage = (ImageView) convertView.findViewById(R.id.item_adv_statistics_image);


        assert statistics != null;
        tvDate.setText(DATE_FORMAT.format(statistics.getDateTime()));
        tvTime.setText(TIME_FORMAT.format(statistics.getDateTime()));
        new UpdateAddressAsync(tvAddress).execute(statistics.getLatitude(), statistics.getLongitude());
        ivImage.setImageBitmap(fileManager.getThumbnails(new File(statistics.getImagePath())));
        ivImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse("file://" + statistics.getImagePath()), "image/*");
                view.getContext().startActivity(intent);
            }
        });
        return convertView;
    }

    private String getAddressString(Double latitude, Double longitude) {
        if (latitude != null && longitude != null) {
            return getAddress(latitude, longitude);
        } else
            return "Местоположение не определено";
    }

    private String getAddress(double latitude, double longitude) {
        Geocoder geocoder;
        List<Address> addresses = new ArrayList<>();
        geocoder = new Geocoder(getContext(), Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (!addresses.isEmpty()) {
            String address = addresses.get(0).getAddressLine(0);
            String city = addresses.get(0).getLocality();
            String adminArea = addresses.get(0).getAdminArea();
            String countryName = addresses.get(0).getCountryName();
            String result = "";
            if (countryName != null)
                result = result.concat(countryName);
            if (adminArea != null)
                result = result.concat(", " + adminArea);
            if (city != null) {
                result = result.concat(", " + city);
            }
            if (address != null)
                result = result.concat(", " + address);
            return result;
        } else {
            return String.format("Широта: %s, долгота: %s", latitude, longitude);
        }
    }


    private class UpdateAddressAsync extends AsyncTask<Double, Void, String> {
        private WeakReference<TextView> weakTextView;

        UpdateAddressAsync(TextView textView) {
            weakTextView = new WeakReference<>(textView);
        }

        @Override
        protected String doInBackground(Double... latLong) {
            return getAddressString(latLong[0], latLong[1]);
        }

        @Override
        protected void onPostExecute(String message) {
            TextView tv = weakTextView.get();
            if (tv != null) {
                tv.setText(message);
            }
        }

    }
}
