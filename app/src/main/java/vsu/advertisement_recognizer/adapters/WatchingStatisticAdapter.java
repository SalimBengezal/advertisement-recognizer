package vsu.advertisement_recognizer.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import vsu.advertisement_recognizer.R;
import vsu.advertisement_recognizer.db.Repository;
import vsu.advertisement_recognizer.db.model.Advertisement;
import vsu.advertisement_recognizer.db.model.Statistics;
import vsu.advertisement_recognizer.manager.FileManager;

/**
 * Класс адаптера списка найденных объектов для экрана наблюдения
 */
public class WatchingStatisticAdapter extends ArrayAdapter<Statistics> {

    private static final DateFormat TIME_FORMAT = SimpleDateFormat.getTimeInstance();
    private static  final String TAG = WatchingStatisticAdapter.class.getName();
    private FileManager fileManager;
    private Repository repository;

    public WatchingStatisticAdapter(Context context, List<Statistics> statisticsList) {
        super(context, 0, statisticsList);
        fileManager = new FileManager(context);
        repository = new Repository();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final Statistics statistics = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_watching_statistics, parent, false);
        }
        TextView tvName = (TextView) convertView.findViewById(R.id.item_watching_statistics_name);
        TextView tvTime = (TextView) convertView.findViewById(R.id.item_watching_statistics_timestamp);
        ImageView ivImage = (ImageView) convertView.findViewById(R.id.item_watching_statistics_image);

        assert statistics!= null;
        final Advertisement advertisement = repository.findAdvertisementById(statistics.getAdvertisementId());
        tvName.setText(advertisement.getName());
        tvTime.setText(TIME_FORMAT.format(statistics.getDateTime()));
        ivImage.setImageBitmap(fileManager.getThumbnails(new File(advertisement.getImagePath())));
        return convertView;
    }
}
