package vsu.advertisement_recognizer.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.List;

import vsu.advertisement_recognizer.R;
import vsu.advertisement_recognizer.db.model.Advertisement;
import vsu.advertisement_recognizer.manager.FileManager;

/**
 * Класс адаптера списка для экрана база рекламы
 */
public class AdvertisementAdapter extends ArrayAdapter<Advertisement> {

    private final FileManager fileManager;

    public AdvertisementAdapter(Context context, List<Advertisement> advertisementList) {
        super(context, 0, advertisementList);
        fileManager = new FileManager(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Advertisement advertisement = getItem(position);
        if (convertView == null)
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_advertisement, parent, false);
        TextView tvName = (TextView) convertView.findViewById(R.id.item_advertisement_title);
        TextView tvDescription = (TextView) convertView.findViewById(R.id.item_advertisement_description);
        ImageView ivImage = (ImageView) convertView.findViewById(R.id.item_advertisement_image);

        assert advertisement != null;
        tvName.setText(advertisement.getName());
        tvDescription.setText(advertisement.getDescription());
        File f = new File(advertisement.getImagePath());
        if (f.exists())
            ivImage.setImageBitmap(fileManager.getThumbnails(f));

        return convertView;
    }

}
