package vsu.advertisement_recognizer;

import android.app.Application;

import vsu.advertisement_recognizer.db.HelperFactory;

/**
 * Класс, дополняющий работу приложения
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        HelperFactory.setHelper(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        HelperFactory.releaseHelper();
        super.onTerminate();
    }
}
