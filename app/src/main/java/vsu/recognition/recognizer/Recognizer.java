package vsu.recognition.recognizer;

import android.util.Log;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.DMatch;
import org.opencv.core.KeyPoint;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgproc.Imgproc;

import java.util.LinkedList;
import java.util.List;

import vsu.recognition.entities.DesiredObject;
import vsu.recognition.entities.Frame;

/**
 * Класс, занимающийся распознаванием объекта на сцене
 */
public class Recognizer {

    private static final String TAG = Recognizer.class.getName();
    private static final FeatureDetector detector = FeatureDetector.create(FeatureDetector.ORB);
    private static final DescriptorExtractor extractor = DescriptorExtractor.create(DescriptorExtractor.ORB);
    private static final DescriptorMatcher matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMING);

    static {
        System.loadLibrary("opencv_java3");
    }

    private final double ratio;
    private final int minGoodPoints;
    private final double homographThreshold;
    private final int borderWidth;
    private final Scalar borderColor;

    /**
     * Конструктор
     *
     * @param ratio              погрешность распознавания
     * @param minGoodPoints      минимальное количество правильных точек
     * @param homographThreshold порог погрешности гомографии
     * @param borderWidth        толщина линии выделения объекта
     * @param borderColor        цвет линии выделения объекта
     */
    public Recognizer(double ratio, int minGoodPoints, double homographThreshold, int borderWidth, Scalar borderColor) {
        this.ratio = ratio;
        this.minGoodPoints = minGoodPoints;
        this.homographThreshold = homographThreshold;
        this.borderWidth = borderWidth;
        this.borderColor = borderColor;

    }

    /**
     * Возвращает список сопоставляемых точек
     *
     * @param frameDescriptor  дескриптор сцены
     * @param objectDescriptor дескриптор объекта
     * @return список сопоставляемых точек
     */
    private static LinkedList<MatOfDMatch> getMatches(MatOfKeyPoint frameDescriptor, MatOfKeyPoint objectDescriptor) {
        LinkedList<MatOfDMatch> matches = new LinkedList<>();
        matcher.knnMatch(frameDescriptor, objectDescriptor, matches, 2);
        return matches;
    }

    /**
     * Возвращает матрицу ключевых точек
     *
     * @param image матрица изображения
     * @return матрица ключевых точек
     */
    public MatOfKeyPoint getKeyPoints(Mat image) {
        MatOfKeyPoint keyPoint = new MatOfKeyPoint();
        detector.detect(image, keyPoint);
        return keyPoint;
    }

    /**
     * Возвращает матрицу дескриптора изображения
     * @param image матрица изображения
     * @param keyPoint матрица ключевых точек
     * @return дескриптор
     */
    public MatOfKeyPoint getDescriptorByKeyPoints(Mat image, MatOfKeyPoint keyPoint) {
        MatOfKeyPoint descriptor = new MatOfKeyPoint();
        extractor.compute(image, keyPoint, descriptor);
        return descriptor;
    }

    /**
     * Возвращает матрицу, содержащую 4 точки границы изображения
     * @param sizeOfObject размер изображения
     * @return матрицу границ изображения
     */
    private Mat getObjectCorners(Size sizeOfObject) {
        Mat matOfCorners = new Mat(4, 1, CvType.CV_32FC2);
        matOfCorners.put(0, 0, 0, 0);
        matOfCorners.put(1, 0, sizeOfObject.width, 0);
        matOfCorners.put(2, 0, sizeOfObject.width, sizeOfObject.height);
        matOfCorners.put(3, 0, 0, sizeOfObject.height);
        return matOfCorners;
    }

    /**
     * Возвращает границы объекта на сцене благодаря гомографии
     * @param objectCorners границы объекта
     * @param homography матрица гомографии
     * @return матрица границ объекта на сцене
     */
    private Mat getMatOfCornersInFrameByHomography(Mat objectCorners, Mat homography) {
        Mat MatOfCornersInFrame = new Mat(4, 1, CvType.CV_32FC2);
        Core.perspectiveTransform(objectCorners, MatOfCornersInFrame, homography);
        return MatOfCornersInFrame;
    }

    /**
     * Возвращает список сопоставляемых точек, удовлетворяющих определенному критерию
     * @param matches список всех сопоставляемых точек
     * @return список сопоставлений точек, удовлетворяющих определенному критерию
     */
    private LinkedList<DMatch> getGoodMatchesList(LinkedList<MatOfDMatch> matches) {
        LinkedList<DMatch> goodMatchesList = new LinkedList<>();
        DMatch[] dMatchArray;
        DMatch m1, m2;
        for (MatOfDMatch match : matches) {
            dMatchArray = match.toArray();
            m1 = dMatchArray[0];
            m2 = dMatchArray[1];
            if (m1.distance <= m2.distance * ratio)
                goodMatchesList.add(m1);
        }
        return goodMatchesList;
    }

    /**
     * Возвращает список соответствующих точек
     * @param frameKeyPoints матрица ключевых точек сцены
     * @param objectKeyPoints матрица ключевых точек объекта
     * @param goodMatchesList список сопоставляемых точек, удовлетворяющих заданному условию
     * @param frameRelevantPoints матрица двойной точности соответствующих точек на сцене
     * @param objectRelevantPoints матрица двойной точности соответствующих точек на объекте
     */
    private void getRelevantPoints(MatOfKeyPoint frameKeyPoints, MatOfKeyPoint objectKeyPoints, LinkedList<DMatch> goodMatchesList, MatOfPoint2f frameRelevantPoints, MatOfPoint2f objectRelevantPoints) {
        LinkedList<Point> framePointList = new LinkedList<>(), objectPointList = new LinkedList<>();
        List<KeyPoint> frameKeyPointsList = frameKeyPoints.toList();
        List<KeyPoint> objectKeyPointsList = objectKeyPoints.toList();
        for (DMatch dMatch : goodMatchesList) {
            int indexFrameKeyPoint = dMatch.queryIdx;
            int indexObjectKeyPoint = dMatch.trainIdx;
            Point framePoint = frameKeyPointsList.get(indexFrameKeyPoint).pt;
            Point objectPoint = objectKeyPointsList.get(indexObjectKeyPoint).pt;
            framePointList.addLast(framePoint);
            objectPointList.addLast(objectPoint);
        }
        frameRelevantPoints.fromList(framePointList);
        objectRelevantPoints.fromList(objectPointList);
    }

    /**
     * Рисует границы объекта на сцене
     * @param image матрица сцены
     * @param corners матрица границ объекта
     */
    private void drawBordersInImage(Mat image, Mat corners) {
        Point[] points = new Point[]{
                new Point(corners.get(0, 0)), new Point(corners.get(1, 0)), new Point(corners.get(2, 0)), new Point(corners.get(3, 0))};
        Imgproc.line(image, points[0], points[1], borderColor, borderWidth);
        Imgproc.line(image, points[1], points[2], borderColor, borderWidth);
        Imgproc.line(image, points[2], points[3], borderColor, borderWidth);
        Imgproc.line(image, points[3], points[0], borderColor, borderWidth);
    }

    /**
     * Возвращает матрицу сцены на котором найден и выделен объект
     * @param frame матрица сцены
     * @param desiredObject матрица объекта
     * @return если найдено, то матрицу сцены, на котором найден и выделен объект, в противном случае - null
     */
    public Mat Run(Frame frame, DesiredObject desiredObject) {
        LinkedList<MatOfDMatch> matches = getMatches(frame.getDescriptor(), desiredObject.getDescriptor());
        LinkedList<DMatch> goodMatchesList = getGoodMatchesList(matches);
        if (goodMatchesList.size() >= minGoodPoints) {
            MatOfPoint2f frameRelevantPoints = new MatOfPoint2f(), objectRelevantPoints = new MatOfPoint2f();
            getRelevantPoints(frame.getKeyPoints(), desiredObject.getKeyPoints(), goodMatchesList, frameRelevantPoints, objectRelevantPoints);
            Mat homography = Calib3d.findHomography(objectRelevantPoints, frameRelevantPoints, Calib3d.RANSAC, homographThreshold);
            Mat objectCorners = getObjectCorners(desiredObject.getSize());
            Mat objectCornersInFrame = getMatOfCornersInFrameByHomography(objectCorners, homography);
            Mat result = frame.getMat().clone();
            drawBordersInImage(result, objectCornersInFrame);
            Log.e(TAG, "FOUND! matches: " + matches.size() + " goodMatchesList: " + goodMatchesList.size());
            return result;
        }
        Log.e(TAG, "NOT_FOUND! matches: " + matches.size() + " goodMatchesList: " + goodMatchesList.size());
        return null;
    }

}
