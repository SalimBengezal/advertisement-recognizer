package vsu.recognition.entities;

import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.Size;

/**
 * Класс, описывающий искомый объект
 */
public class DesiredObject {

    private final Size size;
    private final MatOfKeyPoint keyPoints;
    private final MatOfKeyPoint descriptor;

    /**
     * Конструктор
     *
     * @param size       размерность матрицы
     * @param keyPoints  матрица ключевых точек объекта
     * @param descriptor дескриптор ключевых точек
     */
    public DesiredObject(Size size, MatOfKeyPoint keyPoints, MatOfKeyPoint descriptor) {
        this.size = size;
        this.keyPoints = keyPoints;
        this.descriptor = descriptor;
    }

    /**
     * Возвращает размер изображения
     *
     * @return размер изображения
     */
    public Size getSize() {
        return size;
    }

    /**
     * Возвращает матрицу ключевых точек
     *
     * @return матрица ключевых точек
     */
    public MatOfKeyPoint getKeyPoints() {
        return keyPoints;
    }

    /**
     * Возвращает дескриптор ключевых точек
     * @return дескриптор
     */
    public MatOfKeyPoint getDescriptor() {
        return descriptor;
    }
}
