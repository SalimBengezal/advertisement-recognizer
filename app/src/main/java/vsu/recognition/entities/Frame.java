package vsu.recognition.entities;

import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;

/**
 * Класс, описывающий изображение-сцену, на котором ищется объект
 */
public class Frame {

    private final Mat mat;
    private final MatOfKeyPoint keyPoints;
    private final MatOfKeyPoint descriptor;

    /**
     * Конструктор
     *
     * @param frame      матрица сцены
     * @param keyPoints  ключевые точки сцены
     * @param descriptor дескриптор ключевых точек
     */
    public Frame(Mat frame, MatOfKeyPoint keyPoints, MatOfKeyPoint descriptor) {
        this.mat = frame;
        this.keyPoints = keyPoints;
        this.descriptor = descriptor;
    }

    /**
     * Возвращает матрицу изображения
     *
     * @return матрица изображения
     */
    public Mat getMat() {
        return mat;
    }

    /**
     * Возвращает матрицу ключевых точек
     *
     * @return матрица ключевых точек
     */
    public MatOfKeyPoint getKeyPoints() {
        return keyPoints;
    }

    /**
     * Возвращает дескриптор ключевых точек
     * @return дескриптор
     */
    public MatOfKeyPoint getDescriptor() {
        return descriptor;
    }

}